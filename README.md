Nikki Wines     437324
Fiona Hon       434944

Link to homepage:

http://ec2-54-205-130-75.compute-1.amazonaws.com/~nikkiwines/calendar.php

username: root 
password: root

Creative portion description:

We implemented a tagging function that selects certain events based on a category that the user chooses. 

We also added a event description. Both the tag and the description can be altered by the user. 

In addition to those we also added a reset password feature that allows the user to alter the original password. 

Lastly, we added a “Delete User” portion to our login page where user’s can delete their accounts. It tests against username and password. It will not delete a user that doesn’t already exist in the database. 

